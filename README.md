# Storage Metal Barrels

## Description

Patches some of the metal barrels found in.game to allow storing items inside. No additional recipes or items added.

## Compatibility

- Compatible with Starbound 1.4
- Compatible with Frackin' Universe
- Compatible with Improved Containers (v6)

## Steam Workshop Links

- Storage Metal Barrels: https://steamcommunity.com/sharedfiles/filedetails/?id=1994341567

## Credits

- Me, CC-BY-4.0